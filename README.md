# BBTX

Bibtex reference manager using a fuzzy command line as the primary input method.

![screenshot](bbtx_screenshot.png "bbtx screenshot")

## Installation

Clone the repo, then run `cargo install --path .`

## Usage

BBTX reads a bibtex file for relevant bibtex entries, and adds some metadata
like tags in a separate file. Most interactions are done using a fuzzy command
line which is opened by pressing `space`.

For now, the tool is hard coded to read `main.bib` and read/write to `bbtx.toml`
in the current working directory.

