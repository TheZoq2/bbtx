use crate::bib_entry::TagColor;

pub enum ScrollDirection {
    Up,
    Down
}

#[allow(clippy::large_enum_variant)]
pub enum Msg {
    // Command line related messages
    OpenCommandLine,
    CloseCommandLine,
    AcceptCommandLine,
    // Set the command line value to the current string. If the command line
    // is not opened, it will be
    SetCommandLine(String),

    KeyInput(termion::event::Key),

    Scroll(ScrollDirection),

    ClearFilters,
    SearchTags(Vec<String>),
    SearchTitle(String),

    // Add a tag with the specified name and colour
    CreateTag(String, TagColor),
    // Entry metadata modification messages
    MarkRead(Vec<String>, bool),
    AddTags(String, Vec<String>),
    RemoveTags(String, Vec<String>),
    OpenDoi(String),
    OpenUrl(String),
    /// Start a text editor to edit the notes of the specified 
    EditNotes(String),
    /// Set the notes of the target entry to the speicified notes
    SetNotes(String, Option<String>),

    CopyKey(String),
    CopyTitle(String),

    Exit
}

pub enum Cmd {
    Loopback(Msg),
}
