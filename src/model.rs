use std::collections::HashMap;
use std::io::Stdin;
use std::path::PathBuf;
use std::rc::Rc;
use std::sync::mpsc::{channel, Receiver, Sender, TryRecvError};
use std::sync::{Arc, Mutex};

use anyhow::{Context, Error};
use nom_bibtex::{model::KeyValue, model::StringValueType, Entry};
use webbrowser;

use birch::line_input;
use fzcmd::{expand_command, parse_command, FuzzyOutput, ParseError};
use termion::input::Keys;
use wl_clipboard_rs::copy::{MimeType, Source};

use crate::bib_entry::{
    load_metadata, save_metadata, BibEntry, EntryMetadata, GlobalMetadata, Metadata, TagColor,
};
use crate::command::get_parser;
use crate::msg::{Cmd, ScrollDirection};

pub type CommandLineState = line_input::CommandLineState<String, ParseError>;

const METADATA_FILE: &'static str = "bbtx.toml";

pub struct Model {
    /// Errors shown to the user
    pub errors: Vec<Error>,

    pub scroll_level: i32,

    /// Current tags to filter for
    pub tag_filters: Vec<String>,
    pub title_filter: Option<String>,
    /// Current global metadata
    pub global_metadata: GlobalMetadata,
    /// Bibtex variables
    pub variables: Vec<KeyValue>,
    /// Bibtex entries
    pub bib_entries: Vec<BibEntry>,
    /// State of the fuzzy command line
    pub cli: Option<CommandLineState>,
    /// Hacky way to transmit what to highlight from the fuzzy parser
    pub hl_tx: Rc<Sender<Vec<String>>>,
    pub hl_rx: Rc<Receiver<Vec<String>>>,
    /// Lock around stdin.
    /// Used to stop the key event loop whenever an external editor is open
    pub stdin_keys: Arc<Mutex<Keys<Stdin>>>,
}

impl Model {
    pub fn new(entries: &[Entry], stdin_keys: Arc<Mutex<Keys<Stdin>>>) -> Self {
        let metadata_path = PathBuf::from(METADATA_FILE);
        let metadata = if metadata_path.exists() {
            match load_metadata(&metadata_path) {
                Ok(m) => m,
                Err(e) => {
                    panic!("Loading metadata file failed {}", e)
                }
            }
        } else {
            Default::default()
        };

        let (hl_tx, hl_rx) = channel();
        Self {
            errors: vec![],
            scroll_level: 0,
            variables: vec![],
            bib_entries: vec![],
            cli: None,
            global_metadata: metadata.global,
            tag_filters: vec![],
            title_filter: None,
            hl_tx: Rc::new(hl_tx),
            hl_rx: Rc::new(hl_rx),
            stdin_keys,
        }
        .with_added_entries(&entries, &metadata.entry_data)
    }

    pub fn with_added_entries(
        mut self,
        entries: &[Entry],
        metadata: &HashMap<String, EntryMetadata>,
    ) -> Self {
        for entry in entries {
            match entry {
                Entry::Bibliography(kind, key, vars) => {
                    let vars = vars
                        .iter()
                        .map(|kv| (kv.key.clone(), kv.value.clone()))
                        .collect();
                    self.bib_entries.push(BibEntry {
                        kind: kind.clone(),
                        key: key.clone(),
                        vars,
                        metadata: metadata.get(key).cloned().unwrap_or_else(Default::default),
                    })
                }
                Entry::Variable(var) => self.variables.push(var.clone()),
                _ => {}
            }
        }
        self
    }

    pub fn scroll(self, dir: ScrollDirection) -> Self {
        let scroll_level = match dir {
            ScrollDirection::Up => (self.scroll_level - 1).max(0),
            ScrollDirection::Down => (self.scroll_level + 1).max(0),
        };
        Self {
            scroll_level,
            ..self
        }
    }

    pub fn scroll_top(self) -> Self {
        Self {
            scroll_level: 0,
            ..self
        }
    }

    pub fn open_cli(self) -> Self {
        self.set_cli_input("")
    }

    pub fn close_cli(self) -> Self {
        Self { cli: None, ..self }
    }
    pub fn accept_cli(self) -> (Self, Vec<Cmd>) {
        let parser = get_parser(&self, None);
        match self.cli {
            None => panic!("Accepting CLI input without CLI activated"),
            Some(ref cli) => match parse_command(&cli.fuzzy_expansion, parser) {
                Ok(msg) => (self.close_cli(), vec![Cmd::Loopback(msg)]),
                Err(e) => {
                    let new_cli = CommandLineState {
                        last_error: Some(e),
                        ..cli.clone()
                    };
                    (self.set_cli(Some(new_cli)), vec![])
                }
            },
        }
    }

    pub fn set_cli_input(mut self, input: &str) -> Self {
        self.cli = Some(CommandLineState {
            current_input: input.to_string(),
            ..self.cli.unwrap_or_else(|| CommandLineState::new())
        });
        self.run_fuzzy_parser()
    }

    pub fn set_cli(self, cli: Option<CommandLineState>) -> Self {
        Self { cli, ..self }
    }

    fn run_fuzzy_parser(self) -> Self {
        let parser = get_parser(&self, Some(self.hl_tx.clone()));
        match &self.cli {
            Some(ref cli) => {
                let FuzzyOutput {
                    expanded,
                    suggestions,
                } = expand_command(&cli.current_input, parser);

                let mut new_hl = vec![];
                loop {
                    match self.hl_rx.try_recv() {
                        Ok(val) => new_hl = val,
                        Err(TryRecvError::Empty) => break,
                        Err(TryRecvError::Disconnected) => {
                            panic!("Highlight transmission channel disconnected")
                        }
                    }
                }

                let parsed = parse_command(&expanded, get_parser(&self, None));

                let new_cli = CommandLineState {
                    fuzzy_expansion: expanded,
                    fuzz_suggestions: suggestions.unwrap_or_else(|_| vec![]),
                    last_error: parsed.err(),
                    highlighted: new_hl,
                    ..cli.clone()
                };
                self.set_cli(Some(new_cli))
            }
            None => self,
        }
    }

    pub fn clear_filters(self) -> Self {
        Self {
            tag_filters: vec![],
            title_filter: None,
            ..self
        }
    }

    pub fn set_tag_filter(self, tag_filters: Vec<String>) -> Self {
        Self {
            tag_filters,
            scroll_level: 0,
            ..self
        }
    }

    pub fn set_title_filter(self, title_filter: String) -> Self {
        Self {
            title_filter: Some(title_filter),
            scroll_level: 0,
            ..self
        }
    }

    /// Returns the entries that are currently visible
    pub fn filtered_entries(&self) -> Vec<BibEntry> {
        self.bib_entries
            .iter()
            .cloned()
            .filter(|entry| {
                let matches_tags = self.tag_filters.is_empty()
                    || entry
                        .metadata
                        .tags
                        .iter()
                        .any(|tag| self.tag_filters.contains(&tag));

                let matches_title = self
                    .title_filter
                    .as_ref()
                    .map(|filter| {
                        entry
                            .title()
                            .map(|title| {
                                title.iter().any(|t| match t {
                                    StringValueType::Str(t) => {
                                        t.to_lowercase().contains(&filter.to_lowercase())
                                    }
                                    StringValueType::Abbreviation(_) => false,
                                })
                            })
                            .unwrap_or(false)
                    })
                    .unwrap_or(true);

                matches_tags && matches_title
            })
            .collect::<Vec<_>>()
    }

    /// Marks the specified entries as read or unread
    pub fn set_read_state(self, targets: &[String], state: bool) -> Self {
        self.modify_entry_metadata(targets, |m| m.read = state)
    }

    /// Adds the specified tags to the entry
    pub fn add_tags(self, target: &str, tags: &[String]) -> Self {
        self.modify_entry_metadata(&vec![target.to_string()], |m| {
            for tag in tags {
                m.tags.push(tag.to_string())
            }
        })
    }
    /// Removes the specified tags to the entry
    pub fn remove_tags(self, target: &str, tags: &[String]) -> Self {
        self.modify_entry_metadata(&vec![target.to_string()], |m| {
            m.tags.retain(|t| !tags.contains(t));
        })
    }

    pub fn set_notes(self, target: String, notes: Option<String>) -> Self {
        self.modify_entry_metadata(&[target], |meta| meta.notes = notes.clone())
    }

    /// Adds a new tag that can be added to entries
    pub fn create_tag(self, name: &str, color: &TagColor) -> Self {
        self.modify_global_metadata(|m| m.tags.push((name.to_string(), color.clone())))
    }

    /// Opens the DOI of the specified entry
    pub fn open_doi(mut self, name: &str) -> Self {
        let url = self.get_entry(name).and_then(|entry| entry.doi_url());
        match url {
            Some(Ok(url)) => {
                match webbrowser::open(&url).context("Failed to open browser") {
                    Ok(_) => {}
                    Err(e) => self.errors.push(e),
                }
                self
            }
            Some(Err(e)) => {
                self.errors.push(e);
                self
            }
            None => self,
        }
    }
    /// Opens the URL of the specified entry
    pub fn open_url(mut self, key: &str) -> Self {
        let url = self.get_entry(&key).and_then(|entry| entry.url());
        match url {
            Some(Ok(url)) => {
                match webbrowser::open(&url).context("Failed to open browser") {
                    Ok(_) => {}
                    Err(e) => self.errors.push(e),
                }
                self
            }
            Some(Err(e)) => {
                self.errors.push(e);
                self
            }
            None => {
                self.errors.push(Error::msg("Entry had no URL"));
                self
            }
        }
    }

    /// Copies the bibtex key to the clipboard
    pub fn copy_key(mut self, key: String) -> Self {
        self.copy_text_to_clipboard(key);
        self
    }

    /// Opens the specified entry
    pub fn copy_title(mut self, key: String) -> Self {
        let title = self
            .get_entry(&key)
            .ok_or(Error::msg("No such key"))
            .and_then(|entry| {
                entry
                    .title()
                    .ok_or(Error::msg("Entry does not have a title"))
            });

        match title {
            Ok(title) => {
                if title.len() > 1 {
                    self.errors
                        .push(Error::msg("Copying titles with variables is unsupported"))
                } else if let StringValueType::Str(title) = title[0].clone() {
                    self.copy_text_to_clipboard(title)
                } else {
                    self.errors
                        .push(Error::msg("Copying titles with variables is unsupported"))
                }
                self
            }
            Err(e) => {
                self.errors.push(e);
                self
            }
        }
    }

    fn copy_text_to_clipboard(&mut self, content: String) {
        let opts = wl_clipboard_rs::copy::Options::new();
        opts.copy(Source::Bytes(Box::from(content.as_bytes())), MimeType::Text)
            .map_err(|e| {
                self.errors
                    .push(anyhow::anyhow!("Failed to copy to clipboard {e}"))
            })
            .ok();
    }

    /// Modify the metadata of the specified entry and save the changes to disk
    fn modify_entry_metadata(
        mut self,
        targets: &[String],
        modification: impl Fn(&mut EntryMetadata),
    ) -> Self {
        // NOTE: This could be made much more efficient with a hash map
        for entry in self.bib_entries.iter_mut() {
            if targets.contains(&&entry.key) {
                modification(&mut entry.metadata)
            }
        }
        self.write_metadata()
    }

    /// Modify the global metadata and save the changes to disk
    fn modify_global_metadata(mut self, modification: impl Fn(&mut GlobalMetadata)) -> Self {
        modification(&mut self.global_metadata);
        self.write_metadata()
    }

    pub fn get_entry(&self, target: &str) -> Option<&BibEntry> {
        for entry in self.bib_entries.iter() {
            if entry.key == target {
                return Some(entry);
            }
        }
        None
    }

    pub fn write_metadata(mut self) -> Self {
        let entry_data = self
            .bib_entries
            .iter()
            .map(|e| (e.key.clone(), e.metadata.clone()))
            .collect();

        let metadata = Metadata {
            global: self.global_metadata.clone(),
            entry_data,
        };

        match save_metadata(&PathBuf::from(METADATA_FILE), &metadata) {
            Ok(()) => self,
            Err(e) => {
                self.errors.push(e);
                self
            }
        }
    }

    pub fn with_added_error(mut self, err: anyhow::Error) -> Self {
        self.errors.push(err);
        self
    }
}
