use std::rc::Rc;
use std::sync::mpsc::Sender;
use std::convert::TryInto;

use fzcmd::{Command, ParamGreed, RestQuery};
use lazy_static::lazy_static;
use regex::Regex;

use crate::msg::Msg;
use crate::model::Model;
use crate::bib_entry::TagColor;


fn match_bibtex_key(query: &str) -> (String, String, String, String) {
    lazy_static! {
        static ref RE: Regex = Regex::new(r#"(\s*)(\S*)(\s?)(.*)"#).unwrap();
    }

    let captures = RE.captures_iter(query).next().unwrap();

    (captures[1].into(), captures[2].into(), captures[3].into(), captures[4].into())
}

const GREED_BIBTEX_KEY: ParamGreed = ParamGreed::Custom(&match_bibtex_key);

pub fn get_parser(model: &Model, to_highlight: Option<Rc<Sender<Vec<String>>>>) -> Command<Msg> {
    fn single_arg(
        suggestions: Vec<String>,
        greed: ParamGreed,
        rest_command: Box<dyn Fn(&str) -> Option<Command<Msg>>>,
        highlight_tx: Option<Rc<Sender<Vec<String>>>>,
    ) -> Option<Command<Msg>> {
        Some(Command::NonTerminal(
            greed,
            suggestions,
            Box::new(move |query, _: RestQuery| {
                highlight_tx.as_ref().map(|hl| hl.send(vec![query.to_string()]));
                rest_command(query)
            })
        ))
    }

    fn comma_separated_list<T: Clone>(
        current: Vec<String>,
        suggestions: Vec<String>,
        extra_payload: Rc<T>,
        rest_command: &'static dyn Fn(T, Vec<String>) -> Option<Command<Msg>>,
        highlight_tx: Option<Rc<Sender<Vec<String>>>>,
    ) -> Option<Command<Msg>> {
        Some(Command::NonTerminal(
            ParamGreed::ToComma,
            suggestions.clone(),
            Box::new(move |query, rest| {
                let mut current = current.clone();
                current.push(query.to_string());
                if rest.is_empty() {
                    highlight_tx.as_ref().map(|hl| hl.send(current.clone()));
                    rest_command((*extra_payload).clone(), current)
                }
                else {
                    comma_separated_list(
                        current,
                        suggestions.clone(),
                        extra_payload.clone(),
                        rest_command,
                        highlight_tx.clone(),
                    )
                }
            })
        ))
    }

    fn mark_read(_: (), entries: Vec<String>) -> Option<Command<Msg>> {
        Some(Command::Terminal(Msg::MarkRead(entries, true)))
    }
    fn mark_unread(_: (), entries: Vec<String>) -> Option<Command<Msg>> {
        Some(Command::Terminal(Msg::MarkRead(entries, false)))
    }

    let visible_keys = model.filtered_entries().iter()
        .map(|e| e.key.clone())
        .collect::<Vec<_>>();

    let tag_names = model.global_metadata.tags.iter()
        .map(|(name, _color)| name)
        .cloned()
        .collect::<Vec<_>>();

    Command::NonTerminal(
        ParamGreed::Word,
        vec!(
            "quit".into(),
            "exit".into(),
            "mark_read".into(),
            "mark_unread".into(),
            "new_tag".into(),
            "add_label".into(),
            "remove_label".into(),
            "open_doi".into(),
            "open_url".into(),
            "edit_notes".into(),
            "copy_key".into(),
            "copy_title".into(),
            "filter_tags".into(),
            "filter_title".into(),
            "clear_filters".into(),
        ),
        Box::new(move |query, _| {
            let tag_names = tag_names.clone();
            match query {
                "quit" => {
                    Some(Command::Terminal(Msg::Exit))
                }
                "exit" => {
                    Some(Command::Terminal(Msg::Exit))
                },
                "mark_read" => {
                    comma_separated_list(
                        vec![],
                        visible_keys.clone(),
                        Rc::new(()),
                        &mark_read,
                        to_highlight.clone(),
                    )
                }
                "mark_unread" => {
                    comma_separated_list(
                        vec![],
                        visible_keys.clone(),
                        Rc::new(()),
                        &mark_unread,
                        to_highlight.clone(),
                    )
                }
                "new_tag" => {
                    single_arg(
                        vec![],
                        ParamGreed::ToComma,
                        Box::new(move |name| {
                            let name = name.to_string();
                            single_arg(
                                TagColor::all_colors().iter()
                                    .map(TagColor::as_string)
                                    .collect(),
                                ParamGreed::ToComma,
                                Box::new(move |color| {
                                    Some(Command::Terminal(
                                        Msg::CreateTag(
                                            name.to_string(),
                                            // NOTE: Safe unwrap because
                                            // the suggestions will ensure
                                            // that all colors are valid
                                            color.try_into().unwrap()
                                        )
                                    ))
                                }),
                                None
                            )
                        }),
                        None
                    )
                }
                "add_label" => {
                    single_arg(
                        visible_keys.clone(),
                        GREED_BIBTEX_KEY,
                        Box::new(move |entry| {
                            comma_separated_list(
                                vec![],
                                tag_names.clone(),
                                Rc::new(entry.to_string()),
                                & |entry, tags| {
                                    Some(Command::Terminal(
                                        Msg::AddTags(entry, tags)
                                    ))
                                },
                                None
                            )
                        }),
                        to_highlight.clone()
                    )
                }
                "remove_label" => {
                    single_arg(
                        visible_keys.clone(),
                        GREED_BIBTEX_KEY,
                        Box::new(move |entry| {
                            comma_separated_list(
                                vec![],
                                tag_names.clone(),
                                Rc::new(entry.to_string()),
                                & |entry, tags| {
                                    Some(Command::Terminal(
                                        Msg::RemoveTags(entry, tags)
                                    ))
                                },
                                None
                            )
                        }),
                        to_highlight.clone()
                    )
                }
                "open_doi" => {
                    single_arg(
                        visible_keys.clone(),
                        GREED_BIBTEX_KEY,
                        Box::new(move |entry| {
                            Some(Command::Terminal(
                                Msg::OpenDoi(entry.to_string())
                            ))
                        }),
                        to_highlight.clone()
                    )
                }
                "open_url" => {
                    single_arg(
                        visible_keys.clone(),
                        GREED_BIBTEX_KEY,
                        Box::new(move |entry| {
                            Some(Command::Terminal(
                                Msg::OpenUrl(entry.to_string())
                            ))
                        }),
                        to_highlight.clone()
                    )
                }
                "edit_notes" => {
                    single_arg(
                        visible_keys.clone(),
                        GREED_BIBTEX_KEY,
                        Box::new(move |entry| {
                            Some(Command::Terminal(
                                Msg::EditNotes(entry.to_string())
                            ))
                        }),
                        to_highlight.clone()
                    )
                }
                "copy_key" => {
                    single_arg(
                        visible_keys.clone(),
                        GREED_BIBTEX_KEY,
                        Box::new(move |entry| {
                            Some(Command::Terminal(
                                Msg::CopyKey(entry.to_string())
                            ))
                        }),
                        to_highlight.clone()
                    )
                },
                "copy_title" => {
                    single_arg(
                        visible_keys.clone(),
                        GREED_BIBTEX_KEY,
                        Box::new(move |entry| {
                            Some(Command::Terminal(
                                Msg::CopyTitle(entry.to_string())
                            ))
                        }),
                        to_highlight.clone()
                    )
                }
                "clear_filters" => {
                    Some(Command::Terminal(Msg::ClearFilters))
                },
                "filter_tags" => {
                    comma_separated_list(
                        vec![],
                        tag_names,
                        Rc::new(()),
                        & |_, tags| {
                            Some(Command::Terminal(
                                Msg::SearchTags(tags)
                            ))
                        },
                        None,
                    )
                }
                "filter_title" => {
                    comma_separated_list(
                        vec![],
                        vec![],
                        Rc::new(()),
                        & |_, tags| {
                            Some(Command::Terminal(
                                Msg::SearchTitle(tags.join(" "))
                            ))
                        },
                        None,
                    )
                }
                _ => None
            }
        })
    )
}
