use std::io::Result;
use std::collections::HashMap;
use std::convert::TryInto;

use itertools::Itertools;

use tui::backend::Backend;
use tui::terminal::Frame;
use tui::Terminal;
use tui::widgets::{Widget, Block, Borders, Paragraph, Text, List};
use tui::layout::{Layout, Constraint, Direction, Rect};
use tui::style::{Style, Color, Modifier};
use nom_bibtex::model::StringValueType;
use birch::line_input::draw_command_line;

use crate::model::Model;
use crate::bib_entry::{TagColor, BibEntry};

type TextSection<'a> = (&'a str, Style);

struct TableColumn<'a> {
    pub title: &'a str,
    pub width: u16,
    pub content: &'a dyn Fn(&BibEntry) -> Vec<TextSection>,
}

impl<'a> TableColumn<'a> {
    pub fn new(
        title: &'a str,
        width: u16,
        content: &'a dyn Fn(&BibEntry) -> Vec<TextSection>
    ) -> Self {
        Self {
            title, width, content
        }
    }
}

fn maybe_checked<'a>(
    key: impl 'a + Fn(&BibEntry) -> bool,
    color: Color
) -> impl 'a + Fn(&BibEntry) -> Vec<TextSection> {
    move |e| {
        if key(e) {
            let style = Style::default()
                .fg(color)
                .modifier(Modifier::BOLD);
            vec![("✓", style)]
        }
        else {
            vec![(" ", Style::default())]
        }
    }
}

fn tag_drawer<'a>(
    tags: &'a HashMap<String, TagColor>
) -> impl 'a + Fn(&BibEntry) -> Vec<TextSection> {
    move |e| {
        let mut result = vec![];
        for t in e.metadata.tags.iter() {
            result.push(match tags.get(&t.to_string()) {
                Some(color) => (t.as_str(), Style::default().fg(color.into())),
                None => {
                    warn!(
                        "Entry was marked with tag {}, but it did not exist",
                        t
                    );
                    (t.as_str(), Style::default().fg(Color::White))
                }
            })
        }
        Itertools::intersperse(result.into_iter(), (" ", Style::default()))
            .collect()
    }
}

/// Get the text elements to draw a bibtex string with the variables unexpanded
pub fn maybe_unexpanded_string(s: Option<&[StringValueType]>) -> Vec<TextSection> {
    match s {
        Some(s) => {
            s.iter().map(|segment| {
                match segment {
                    StringValueType::Abbreviation(abbrv) => {
                        (
                            abbrv.as_str(),
                            Style::default().fg(Color::Gray)
                        )
                    }
                    StringValueType::Str(val) => {
                        (
                            val.as_str(),
                            Style::default()
                        )
                    }
                }
            })
            .collect()
        }
        None => {
            vec![("-", Style::default().fg(Color::Gray))]
        }
    }
}

/**
  Draws the specified list of strings into a region of at most max_len.
  If the strings do not fit in that region, the last character will be
  replaced by "…"
*/
fn draw_key<'a>(
    value: &'a[TextSection<'a>],
    max_len: usize,
    highlight: bool
) -> Vec<Text> {
    let mut result = vec![];
    let mut current_length = 0;
    let mut overflowed = false;
    let hidden_style = Style::default().fg(Color::Gray);
    for (text, style) in value {
        // Length of this segment
        let len = text.chars().count();
        // Remaining length
        let remaining = max_len - current_length;
        // Check if the whole string will fit. If not, remember that and take
        // one less character
        let amount_to_take = if len > remaining {
            overflowed = true;
            remaining - 1
        }
        else {
            remaining
        };
        let new_chars = text.chars()
            .take(amount_to_take)
            .collect::<String>();
        current_length += new_chars.len();
        let actual_style = if highlight {*style} else {hidden_style};
        result.push(Text::styled(new_chars, actual_style));
    }
    if overflowed {
        result.push(Text::styled("…", Style::default().fg(Color::Gray)));
    }
    result
}

fn draw_entry(
    f: &mut Frame<impl Backend>,
    area: Rect,
    entry: &BibEntry,
    columns: &[TableColumn],
    highlighted_entries: &[String],
) {
    // +1 for border
    let constraints = columns.iter().map(|c| Constraint::Length(c.width + 1));
    let chunks = Layout::default()
        .direction(Direction::Horizontal)
        .margin(0)
        .constraints(constraints.collect::<Vec<_>>())
        .split(area);

    let should_hl = highlighted_entries.is_empty()
        || highlighted_entries.contains(&entry.key);

    for (column, a) in columns.iter().zip(chunks) {
        let content = (column.content)(entry);

        let all_fragments = draw_key(&content, column.width as usize, should_hl)
            .into_iter()
            .collect::<Vec<_>>();

        let mut block = Block::default()
             .border_style(Style::default().fg(Color::Gray))
             .borders(Borders::RIGHT);

        Paragraph::new(all_fragments.iter())
            .wrap(true)
            .render(f, block.inner(a));
        block.render(f, a)
    }
}

pub fn draw_entries(
    f: &mut Frame<impl Backend>,
    area: Rect,
    entries: &[BibEntry],
    highlighted_entries: &[String],
    tags: &HashMap<String, TagColor>
) {
    let read_check = maybe_checked(|e| e.metadata.read, Color::Green);
    let doi_check = maybe_checked(|e| e.doi_url().is_some(), Color::LightBlue);
    let notes_check = maybe_checked(|e| e.metadata.notes.is_some(), Color::Magenta);
    let tag_drawer_ = tag_drawer(tags);
    let columns = vec![
        TableColumn::new("Key", 25, &|e| vec![(&e.key, Style::default())]),
        TableColumn::new("R", 1, &read_check),
        TableColumn::new("D", 1, &doi_check),
        TableColumn::new("N", 1, &notes_check),
        TableColumn::new("Title", 80, &|e| maybe_unexpanded_string(e.title())),
        TableColumn::new("Authors", 20, &|e| maybe_unexpanded_string(e.author())),
        TableColumn::new("Tags", 40, &tag_drawer_)
    ];
    let constraints =
        (0..=entries.len()+1).map(|_| Constraint::Length(2));
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .margin(0)
        .constraints(constraints.collect::<Vec<_>>())
        .split(area);

    // Draw the table header
    {
        let constraints = columns.iter().map(|c| Constraint::Length(c.width+1));
        let chunks = Layout::default()
            .direction(Direction::Horizontal)
            .margin(0)
            .constraints(constraints.collect::<Vec<_>>())
            .split(chunks[0]);

        for (column, chunk) in columns.iter().zip(chunks) {
            let style = Style::default().modifier(Modifier::BOLD);
            let text = vec![Text::styled(column.title, style)];
            Paragraph::new(text.iter())
                .render(f, chunk);
            Block::default()
                 .border_style(Style::default().fg(Color::Gray))
                 .borders(Borders::RIGHT)
                 .render(f, chunk);
        }
    }

    // Draw the rest of the table
    for (entry, chunk) in entries.iter().zip(chunks.into_iter().skip(1)) {
        draw_entry(f, chunk, entry, &columns, highlighted_entries);
    }
}

// Draws the main content. Returns the location of the cursor if it should
// be rendered
pub fn draw_main(f: &mut Frame<impl Backend>, area: Rect, model: &Model) -> Option<(u16, u16)>{
    let tag_map = model.global_metadata.tags.iter().cloned().collect();

    let filtered_entries = model.filtered_entries();

    let entries_to_show = filtered_entries.into_iter()
        .rev()
        .skip(model.scroll_level.try_into().unwrap())
        .collect::<Vec<_>>();

    match &model.cli {
        Some(cli) => {
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .constraints([
                    Constraint::Min(1),
                    Constraint::Length(10)
                ].as_ref())
                .split(area);

            draw_entries(f, chunks[0], &entries_to_show, &cli.highlighted, &tag_map);
            let (cursor_x, cursor_y) = draw_command_line(f, chunks[1], cli);
            Some((cursor_x + area.left(), cursor_y + area.top()))
        }
        None => {
            draw_entries(f, area, &entries_to_show, &[], &tag_map);
            None
        }
    }
}

// Draw the whole TUI. Returns the location to put the cursor after
// drawing is finished
pub fn draw(terminal: &mut Terminal<impl Backend>, model: &Model) -> Result<()>{
    let mut cursor_position = None;
    terminal.draw(|mut f| {
        let errors_to_draw = &model.errors.iter()
            .rev()
            .take(5)
            .collect::<Vec<_>>();
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints([
                Constraint::Length(errors_to_draw.len() as u16),
                Constraint::Min(1),
            ].as_ref())
            .split(f.size());


        List::new(errors_to_draw.iter().
                map(|ref e| {
                    Text::styled(
                        (*e).to_string(),
                        Style::default().fg(Color::Red)
                    )
                })
            )
            .render(&mut f, chunks[0]);
        cursor_position = draw_main(&mut f, chunks[1], model);
    })?;

    match cursor_position {
        Some(pos) => {
            terminal.show_cursor()?;
            terminal.set_cursor(pos.0, pos.1)
        },
        None => terminal.hide_cursor()
    }
}
