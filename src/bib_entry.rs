use std::collections::HashMap;
use std::path::Path;
use std::fs::File;
use std::io::prelude::*;

use anyhow::{Context, Result, bail};

use nom_bibtex::model::StringValueType;

use serde_derive::{Serialize, Deserialize};
use toml;

macro_rules! define_tag_color {
    ($name:ident { $( $color:ident ),*$(,)? } ) => {
        // Define the enum itself
        #[derive(Serialize, Deserialize, Debug, Clone)]
        pub enum $name {
            $( $color ),*
        }

        // Define the conversion to tui::Color
        impl From<&$name> for tui::style::Color {
            fn from(other: &$name) -> tui::style::Color {
                match other {
                    $($name::$color => tui::style::Color::$color),*
                }
            }
        }

        impl std::convert::TryFrom<&str> for $name {
            type Error = ();
            fn try_from(value: &str) -> Result<Self, Self::Error> {
                match value {
                    $( stringify!($color) => Ok($name::$color ) ),*,
                    _ => Err(())
                }
            }
        }

        impl $name {
            pub fn as_string(&self) -> String {
                match self {
                    $( $name::$color => stringify!($color).to_string() ),*
                }
            }

            pub fn all_colors() -> Vec<Self> {
                vec![ $( $name::$color ),* ]
            }
        }
    }
}
define_tag_color!(TagColor {
    Black,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    Gray,
    DarkGray,
    LightRed,
    LightGreen,
    LightYellow,
    LightBlue,
    LightMagenta,
    LightCyan,
    White,
});



#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Metadata {
    pub global: GlobalMetadata,
    pub entry_data: HashMap<String, EntryMetadata>,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct GlobalMetadata {
    pub tags: Vec<(String, TagColor)>,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct EntryMetadata {
    pub tags: Vec<String>,
    pub read: bool,
    pub notes: Option<String>,
}

pub fn save_metadata(filename: &Path, data: &Metadata) -> Result<()> {
    let to_write = toml::to_string(&data).context("Failed to encode data")?;

    let mut file = File::create(filename)
        .with_context(|| format!("Failed to open {:?}", filename))?;

    file.write_all(to_write.as_bytes())
        .with_context(|| format!("Failed to write metadata to {:?}", filename))?;

    Ok(())
}
pub fn load_metadata(filename: &Path) -> Result<Metadata> {
    let mut file = File::open(filename)
        .with_context(|| format!("Failed to open metadata file: {:?}", filename))?;

    let mut content = String::new();
    file.read_to_string(&mut content)
        .with_context(|| format!("Failed to read metadata file: {:?}", filename))?;

    toml::from_str(&content)
        .with_context(|| format!("Failed to parse {:?}", filename))
}


#[derive(Clone, Debug)]
pub struct BibEntry {
    pub kind: String,
    pub key: String,
    pub vars: HashMap<String, Vec<StringValueType>>,
    pub metadata: EntryMetadata,
}

impl BibEntry {
    pub fn title(&self) -> Option<&[StringValueType]> {
        self.vars.get("title").map(AsRef::as_ref)
    }
    pub fn author(&self) -> Option<&[StringValueType]> {
        self.vars.get("author").map(AsRef::as_ref)
    }
    /// Returns the DOI url of the entry. Returns None if it is
    /// not defined, and Some(Err) if the url contains abbreviations
    pub fn doi_url(&self) -> Option<Result<String>> {
        self.vars.get("doi")
            .map(|raw| {
                match raw.first() {
                    Some(StringValueType::Str(val)) => {
                        if val.starts_with("http") {
                            Ok(val.to_string())
                        }
                        else {
                            Ok(format!("http://dx.doi.org/{}", val))
                        }
                    }
                    _ => bail!("Abbreviations in DOI strings are unsupported")
                }
            })
    }

    /// Returns the url of the entry. Returns None if it is
    /// not defined, and Some(Err) if the url contains abbreviations
    pub fn url(&self) -> Option<Result<String>> {
        self.vars.get("url")
            .map(|raw| {
                match raw.first() {
                    Some(StringValueType::Str(val)) => {
                        Ok(val.to_string())
                    }
                    _ => bail!("Abbreviations in DOI strings are unsupported")
                }
            })
    }
}

