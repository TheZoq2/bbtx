
use std::fs::File;
use std::io::{self, prelude::*};
use std::path::PathBuf;
use std::sync::{Mutex, Arc};

#[macro_use] extern crate log;
use simplelog::{CombinedLogger, WriteLogger};
use nom_bibtex::{self, Entry, error::BibtexError, Bibtex};

use termion::event::Key;
use termion::raw::IntoRawMode;
use tui::Terminal;
use tui::backend::TermionBackend;
use termion::input::TermRead;

use birch::event::{Event, Events};
use birch::line_input::handle_key_input;
use birch::text_editor::edit_text;

mod model;
mod msg;
mod view;
mod bib_entry;
mod command;

use model::Model;
use msg::{Msg, Cmd, ScrollDirection};


// TODO: Consider exporting this to birch
macro_rules! loopback {
    ($msg:expr) => (vec![Cmd::Loopback($msg)])
}


fn read_bibtex_file() -> Vec<Entry>{
    let files = vec![
        "vars/months.bib",
        "/usr/share/texmf-dist/bibtex/bib/ieeetran/IEEEabrv.bib",
        "main.bib",
    ];

    files.iter()
        .map(|filename| {
            let mut file = File::open(filename)
                .expect(&format!("Failed to open {}", filename));
            let mut content = vec!();
            file.read_to_end(&mut content)
                .expect(&format!("Failed to read content of {}", filename));
            let file_content = String::from_utf8_lossy(&content);

            println!("Parsing {}", filename);
            match Bibtex::raw_parse(&file_content) {
                Ok(entries) => entries,
                Err(BibtexError::Parsing(e)) => {
                    panic!("Parse error in {}:\n{}", filename, e)
                }
                Err(e) => {Err(e).unwrap()}
            }
        })
        .fold(vec![], |mut acc, mut entries| {
            acc.append(&mut entries);
            acc
        })
}

fn try_edit_notes(model: Model, target: String) -> (Model, Vec<Cmd>) {
    let current_description = model.get_entry(&target)
        .expect("No entry for target")
        .metadata.notes
        .clone()
        .unwrap_or_else(|| "".to_string());

    let result_msg = edit_text(
        model.stdin_keys.clone(),
        "/tmp/bbtx",
        "md",
        &current_description,
        move |new_text| {
            let result = if new_text != "" {Some(new_text)} else {None};
            Msg::SetNotes(target.clone(), result)
        }
    );

    match result_msg {
        Ok(msg) => (model, vec!(Cmd::Loopback(msg))),
        Err(e) => {
            (model.with_added_error(e.into()), vec!())
        }
    }
}


fn update(msg: Msg, model: Model) -> (Model, Vec<Cmd>) {
    match msg {
        Msg::Exit => {
            unreachable!("Should be caught in event loop")
        },
        Msg::KeyInput(key) => {
            // Check if this is a global command
            if key == Key::Ctrl('c') || key == Key::Ctrl('d') {
                (model, loopback!(Msg::Exit))
            }
            else if let Some(ref cli) = model.cli {
                let result = handle_key_input(
                        cli.current_input.clone(),
                        key,
                        Msg::CloseCommandLine,
                        Msg::AcceptCommandLine,
                        |s| Msg::SetCommandLine(s.to_string()),
                    )
                    .map(|msg| loopback!(msg))
                    .unwrap_or_else(|| vec![]);
                (
                    model,
                    result
                )
            }
            else {
                match key {
                    Key::Char(' ') => (model, vec![Cmd::Loopback(Msg::OpenCommandLine)]),
                    Key::Char('j') =>
                        (model, loopback!(Msg::Scroll(ScrollDirection::Down))),
                    Key::Char('k') =>
                        (model, loopback!(Msg::Scroll(ScrollDirection::Up))),
                    Key::Char('g') =>
                        (model.scroll_top(), vec![]),
                    Key::Char('q') =>
                        (model, loopback!(Msg::Exit)),
                    _ => (model, vec![])
                }
            }
        }
        Msg::OpenCommandLine => (model.open_cli(), vec![]),
        Msg::CloseCommandLine => (model.close_cli(), vec![]),
        Msg::AcceptCommandLine => model.accept_cli(),
        Msg::SetCommandLine(s) => (model.set_cli_input(&s), vec![]),
        Msg::EditNotes(target) => try_edit_notes(model, target),
        Msg::SetNotes(target, notes) => (model.set_notes(target, notes), vec![]),
        Msg::MarkRead(targets, s) => (model.set_read_state(&targets, s), vec![]),
        Msg::CreateTag(name, color) => (model.create_tag(&name, &color), vec![]),
        Msg::AddTags(target, tags) => (model.add_tags(&target, &tags), vec![]),
        Msg::RemoveTags(target, tags) => (model.remove_tags(&target, &tags), vec![]),
        Msg::OpenDoi(target) => (model.open_doi(&target), vec![]),
        Msg::OpenUrl(target) => (model.open_url(&target), vec![]),
        Msg::CopyKey(key) => {(model.copy_key(key), vec![])},
        Msg::CopyTitle(key) => {(model.copy_title(key), vec![])},
        Msg::SearchTags(tags) => (model.set_tag_filter(tags), vec![]),
        Msg::SearchTitle(title) => (model.set_title_filter(title), vec![]),
        Msg::ClearFilters => (model.clear_filters(), vec![]),
        Msg::Scroll(dir) => (model.scroll(dir), vec![]),
    }
}


fn main() -> Result<(), io::Error> {
    let log_file = PathBuf::from(
        format!("/tmp/bbtx/{}.log", std::process::id())
    );
    match std::fs::create_dir_all(log_file.parent().unwrap()) {
        Ok(_) => {
            println!(
                "Logging to {:?}, run `cat bbtx.log` to start if it is a fifo",
                log_file
            );
            let log_result = CombinedLogger::init(
                vec![WriteLogger::new(
                    simplelog::LevelFilter::Info,
                    simplelog::Config::default(),
                    std::fs::File::create(log_file).unwrap()
                )]
            );
            if let Err(e) = log_result {
                println!("Logger failed to initialise. Logging disabled");
                println!("Error: {:?}", e);
            }
        }
        Err(e) => {
            println!("Failed to create dir for log storage. Logging disabled");
            println!("Error: {:?}", e);
        }
    }


    // Set up stdin
    let stdin_keys = Arc::new(Mutex::new(io::stdin().keys()));

    // Set up the model
    let entries = read_bibtex_file();
    let mut model = Model::new(&entries, stdin_keys.clone());

    // Set up termion
    let stdout = io::stdout().into_raw_mode()?;
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    terminal.clear()?;

    let events = Events::new(stdin_keys);

    // Messages received in this iteration. Add messages that should be
    // send in the first iteration here.
    let mut msgs = vec!();
    'main: loop {
        // Check input
        #[allow(clippy::single_match)]
        match events.next().unwrap() {
            Event::Input(key) => {
                msgs.push(Msg::KeyInput(key));
            }
            _ => {}
        }

        // Update the model with all new messages
        while let Some(msg) = msgs.pop() {
            // Exit messages have to be handled here, otherwise the terminal
            // does not get cleaned up
            if let Msg::Exit = msg {
                break 'main Ok(());
            }
            let (new_model, new_cmds) = update(msg, model);
            model = new_model;
            // Run commands
            for cmd in new_cmds {
                match cmd {
                    Cmd::Loopback(msg) => msgs.push(msg),
                }
            }
        }

        view::draw(&mut terminal, &model)?;
    }
}
